//
//  CreditDetailsViewController.swift
//  Moobee
//
//  Created by Toni García Alhambra on 25/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class CreditDetailsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var backgroundView: UIView!
    
    @IBOutlet weak var castCollectionView: UICollectionView!
    
    var cast = [cast_childCredtis]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        castCollectionView.delegate = self
        castCollectionView.dataSource = self
        
        castCollectionView.register(UINib(nibName: "CastCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "castCell")
        
        setUI()
    }
    
    func setUI() {
        let columnLayout = ColumnFlowLayout(
            cellsPerRow: 3,
            minimumInteritemSpacing: 3,
            minimumLineSpacing: 3,
            sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        )
        
        castCollectionView.collectionViewLayout = columnLayout
        
        setColors()
    }
    
    func setColors() {
        backgroundView.backgroundColor = ColorsForDarkVersion.main
        castCollectionView.backgroundColor = ColorsForDarkVersion.main
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cast.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "castCell", for: indexPath) as! CastCollectionViewCell
        
        let node: Character = "/"
        let characterName = cast[indexPath.row].character!
        
        // Corta el string si tiene varios nombres. ej: Carol/Cap. Marvel -> Carol
        if let index = characterName.index(of: node) {
            let pos = characterName.distance(from: characterName.startIndex, to: index)
            cell.characterNameLabel.text = String((characterName.prefix(pos)))
        } else {
            cell.characterNameLabel.text = characterName
        }
        
        cell.nameLabel.text = cast[indexPath.row].name
        
        if let profile_pic = cast[indexPath.row].profile_pic {
            cell.profilePic.image = profile_pic
        }
        
        return cell
    }
}
