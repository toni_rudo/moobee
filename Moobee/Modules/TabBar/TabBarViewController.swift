//
//  TabBarViewController.swift
//  Moobee
//
//  Created by Toni García Alhambra on 14/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let tabBarItems = tabBar.items! as [UITabBarItem]
        
        for (index, _) in tabBarItems.enumerated() {
            setNavigation(index)
        }
        
        setUI()
    }
    
    func setUI() {
        setColors()
    }
    
    func setColors() {
        tabBar.barTintColor = ColorsForDarkVersion.main
        tabBar.tintColor = ColorsForDarkVersion.emphasis
        
        UINavigationBar.appearance().barTintColor = ColorsForDarkVersion.main
        UINavigationBar.appearance().shadowImage = UIImage()
    }
    
    func setNavigation(_ index: Int){
        let nav = viewControllers![index] as? UINavigationController
        
        switch index {
        case 0:
            nav?.viewControllers.removeAll()
            break
            
        case 1:
            nav?.viewControllers = [SearchViewController.instantiate(fromAppStoryboard: .Search)]
            break
            
        case 2:
            break
            
        case 3:
            break
            
        default:
            break
        }
    }
}
