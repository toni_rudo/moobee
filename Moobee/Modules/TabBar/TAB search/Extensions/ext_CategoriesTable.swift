//
//  ext_SearchTableView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 15/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

extension CategoriesTableViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].name
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        
        headerView.textLabel?.font = headerView.textLabel?.font.withSize(30)
        headerView.textLabel?.textColor = UIColor.white
        headerView.backgroundView?.backgroundColor = ColorsForDarkVersion.main
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "moviesTableCell", for: indexPath) as! MoviesTableViewCell
        
        if let movies = movies[sections[indexPath.section].endPoint] {
            cell.movies = movies
            cell.parentController = self
        }
        
        return cell
    }
}
