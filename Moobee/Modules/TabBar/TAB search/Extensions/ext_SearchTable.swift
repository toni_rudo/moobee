//
//  ext_SearchTable.swift
//  Moobee
//
//  Created by Toni García Alhambra on 22/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

extension SearchTableViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moviesSearched.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchedCell") as! SearchedMovieTableViewCell
        
        if moviesSearched.count > 0 && moviesSearched[indexPath.row].smallPosterImage != nil {
            let movie = moviesSearched[indexPath.row]
            cell.titleLabel.text = movie.title
            cell.dateLabel.text = movie.release_date
            cell.posterImageView.image = movie.smallPosterImage
            cell.cellButton.tag = movie.id ?? 0
            cell.cellButton.addTarget(self, action: #selector(push(sender:)), for: .touchUpInside)
        }
        
        return cell
    }
}
