//
//  ext_SearchView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 22/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

extension SearchViewController: SearchView {
    func setMovies(_ movies: [Movie]) {
        containerViewController?.moviesSearched = movies
    }
}
