//
//  extSearchView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 15/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

extension CategoriesTableViewController: CategoriesTableView {
    func reloadSection(_ section: EndPoint) {
        let index = IndexPath(row: 0, section: self.endPoints.firstIndex(of: section)!)
        
        categoriesTable.reloadRows(at: [index], with: UITableView.RowAnimation.none)
    }
    
    func setMovies(endPoint: EndPoint, movies: [Movie]) {
        self.movies[endPoint] = movies
    }
}
