//
//  SearchTablePresenter.swift
//  Moobee
//
//  Created by Toni García Alhambra on 21/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class SearchPresenter {
    
    weak private var searchView: SearchView!
    
    func attachView(view: SearchView){
        searchView = view
    }
    
    func searchMovie(withString string: String) {
        Movie.searchMovies(withString: string) { (movies) in
            if movies.count > 0 {
                self.searchView.setMovies(movies)
            }
        }
    }
    
}
