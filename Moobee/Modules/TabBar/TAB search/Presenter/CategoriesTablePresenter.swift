//
//  CategoriesTablePresenter.swift
//  Moobee
//
//  Created by Toni García Alhambra on 21/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class CategoriesTablePresenter {
    weak private var categoriesTableView: CategoriesTableView!
    
    func attachView(view: CategoriesTableView){
        categoriesTableView = view
    }
    
    func getMovies(sections: [EndPoint]) {
        for path in sections {
            switch path {
            case .discover :
                DiscoveredMovie.getDiscoveredMovies { (movies) in
                    self.set(endPoint: path, movies: movies.results)
                }
            case .nowPlaying:
                MovieNowPlaying.getMoviesInTheatres { (movies) in
                    self.set(endPoint: path, movies: movies.results)
                }
            case .topRated:
                MovieTopRated.getTopRatedMovies { (movies) in
                    self.set(endPoint: path, movies: movies.results)
                }
            }
        }
    }
    
    func set(endPoint path: EndPoint, movies: [Movie]) {
        self.addImageTo(endPoint: path, movies: movies)
        self.categoriesTableView.setMovies(endPoint: path, movies: movies)
    }
    
    func addImageTo(endPoint: EndPoint, movies: [Movie]) {
        for movie in movies {
            Alamofire.request("\(Path.imagesSmallSize)\(movie.poster_path ?? "")").responseImage { response in
                if let image = response.result.value {
                    movie.smallPosterImage = image
                    self.categoriesTableView.reloadSection(endPoint)
                }
            }
        }
    }
}
