//
//  SearchTablePresenter.swift
//  Moobee
//
//  Created by Toni García Alhambra on 22/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class SearchTablePresenter {
    
    weak private var searchTableView: SearchTableViewController! // No necesita protocolo
    
    func attachView(view: SearchTableViewController){
        searchTableView = view
    }
    
    func getPosters(from movies: [Movie]) {
        for movie in movies {
            Alamofire.request("\(Path.imagesSmallSize)\(movie.poster_path ?? "")").responseImage { response in
                if let image = response.result.value {
                    movie.smallPosterImage = image
                    
                    self.searchTableView.searchTableView.reloadData()
                }
            }
        }
    }
    
}
