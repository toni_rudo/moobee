//
//  Sections.swift
//  Moobee
//
//  Created by Toni García Alhambra on 14/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import Foundation

enum EndPoint {
    case discover,
         nowPlaying,
         topRated
}

struct Section {
    var name: String
    var endPoint: EndPoint
}

struct Sections {
    let sections: [Section] = [
        Section.init(name: "Populares del momento", endPoint: .discover),
        Section.init(name: "En cines", endPoint: .nowPlaying),
        Section.init(name: "Mejor valoradas", endPoint: .topRated)
    ]
}
