//
//  SearchView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 15/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

protocol CategoriesTableView: class {
    func setMovies(endPoint: EndPoint, movies: [Movie])
    func reloadSection(_ section: EndPoint)
}
