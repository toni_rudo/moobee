//
//  SearchTableView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 21/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

protocol SearchView: class {
    func setMovies(_ movies: [Movie])
}
