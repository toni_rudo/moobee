//
//  CategoriesTableViewController.swift
//  Moobee
//
//  Created by Toni García Alhambra on 21/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class CategoriesTableViewController: UIViewController {
    @IBOutlet weak var categoriesTable: UITableView!
    
    let sections = Sections().sections
    
    let categoriesTablePresenter = CategoriesTablePresenter()
    
    var movies = [EndPoint: [Movie]]() {
        didSet {
            categoriesTable.reloadData()
        }
    }
    var endPoints = [EndPoint]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        categoriesTablePresenter.attachView(view: self)
        
        categoriesTable.delegate = self
        categoriesTable.dataSource = self
        
        categoriesTable.register(UINib(nibName: "MoviesTableViewCell", bundle: nil), forCellReuseIdentifier: "moviesTableCell")
        
        for section in sections {
            endPoints.append(section.endPoint)
        }
        
        setColors()
        
        
        self.categoriesTablePresenter.getMovies(sections: self.endPoints)
    }
    
    func setColors() {
        categoriesTable.backgroundColor = ColorsForDarkVersion.main
        categoriesTable.sectionIndexBackgroundColor = ColorsForDarkVersion.emphasis
    }
}
