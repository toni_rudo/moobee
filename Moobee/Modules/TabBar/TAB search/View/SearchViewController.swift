//
//  SearchViewController.swift
//  Moobee
//
//  Created by Toni García Alhambra on 14/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet weak var searchContainer: UIView!
    @IBOutlet weak var categoriesContainer: UIView!
    
    lazy var searchBar: UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
    
    let searchPresenter = SearchPresenter()
    
    var containerViewController: SearchTableViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchPresenter.attachView(view: self)
        
        setUI()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "searchViewEmbedSegue" {
            containerViewController = segue.destination as? SearchTableViewController
        }
    }
    
    func setUI() {
        searchBar.delegate = self
        
        searchBar.sizeToFit()
        searchBar.placeholder = "Buscar películas"
        searchBar.setValue("Cancelar", forKey:"_cancelButtonText")
        
        navigationItem.titleView = searchBar
        setColors()
    }
    
    func setColors() {
        searchBar.setText(color: UIColor.white)
        searchBar.setTextField(color: ColorsForDarkVersion.secondary)
        searchBar.setPlaceholderText(color: ColorsForDarkVersion.light)
        searchBar.setSearchImage(color: ColorsForDarkVersion.light)
        searchBar.setClearButton(color: ColorsForDarkVersion.light)
        searchBar.tintColor = UIColor.white
        
        self.view.backgroundColor = ColorsForDarkVersion.main
    }
}
