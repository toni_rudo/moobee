//
//  SearchTableViewController.swift
//  Moobee
//
//  Created by Toni García Alhambra on 21/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class SearchTableViewController: UIViewController {
    
    @IBOutlet weak var searchTableView: UITableView!
    
    let searchTablePresenter = SearchTablePresenter()
    
    var moviesSearched = [Movie]() {
        didSet {
            if moviesSearched.count == 0 {
                searchTableView.reloadData()
            } else {
                searchTablePresenter.getPosters(from: moviesSearched)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchTablePresenter.attachView(view: self)
        
        searchTableView.delegate = self
        searchTableView.dataSource = self
        searchTableView.register(UINib(nibName: "SearchedMovieTableViewCell", bundle: nil), forCellReuseIdentifier: "searchedCell")
        searchTableView.keyboardDismissMode = .onDrag
        
        setColors()
    }
    
    func setColors() {
        searchTableView.backgroundColor = ColorsForDarkVersion.main
        searchTableView.separatorColor = ColorsForDarkVersion.secondary
    }
    
    @objc func push(sender: AnyObject) {
        let movieVC = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
        movieVC.id = sender.tag
        
        self.navigationController?.pushViewController(movieVC, animated: true)
    }
}
