//
//  WellcomeRegisterPresenter.swift
//  Moobee
//
//  Created by Toni García Alhambra on 25/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class RegisterNamePresenter {
    
    // MARK: - Properties
    weak private var registerNameView: RegisterNameView!
    
    func attachView(view: RegisterNameView){
        registerNameView = view
    }
    
    func checkName(nameField: UITextField) {
        if nameField.text == "" {
            registerNameView.setName("¡Hola!")
        } else {
            if nameField.text!.count > 15 {
                nameField.deleteBackward()
            } else {
                registerNameView.setName("¡Hola, \(nameField.text ?? "")!")
            }
        }
    }
}
