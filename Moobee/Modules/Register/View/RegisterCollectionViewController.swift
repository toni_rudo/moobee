//
//  RegisterCollectionViewController.swift
//  Moobee
//
//  Created by Toni García Alhambra on 24/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var registerCollectionView: UICollectionView!
    @IBOutlet weak var nextButton: UIButton!
    
    let nameCell = "registerNameCell"
    let credentialsCell = "registerCredentialsCell"
    
    private var currentCell: Int = 0
    var user = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerCollectionView.delegate = self
        registerCollectionView.dataSource = self
        
        registerCollectionView.register(UINib(nibName: "RegisterNameCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: nameCell)
        registerCollectionView.register(UINib(nibName: "RegisterCredentialsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: credentialsCell)
        
        backgroundView.backgroundColor = ColorsForDarkVersion.main.withAlphaComponent(0.9)
        
        nextButton.backgroundColor = ColorsForDarkVersion.emphasis
        nextButton.layer.cornerRadius = nextButton.frame.height / 2
    }
    
    @IBAction func registerCollectionView(_ sender: UIButton) {
        
        let currentIndexPath = IndexPath(item: currentCell, section: 0)
        let nextIndexPath = IndexPath(item: currentCell + 1, section: 0)
        
        switch currentCell {
            case 0:
                let cell = self.registerCollectionView.cellForItem(at: currentIndexPath) as! RegisterNameCollectionViewCell
                
                user.name = cell.nameTextField.text
                user.username = cell.usernameTextField.text
            default: break
        }
        
        self.registerCollectionView.scrollToItem(at: nextIndexPath, at: [.centeredVertically, .centeredHorizontally], animated: true)
        currentCell += 1
    }
}
