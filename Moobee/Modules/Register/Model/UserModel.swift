//
//  UserModel.swift
//  Moobee
//
//  Created by Toni García Alhambra on 25/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import Foundation

struct User {
    var name, email, password, headerPic, username: String?
}
