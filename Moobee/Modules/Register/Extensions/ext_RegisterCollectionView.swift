//
//  ext_RegisterCollectionView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 25/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

extension RegisterViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.row {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: nameCell, for: indexPath) as! RegisterNameCollectionViewCell
            
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: credentialsCell, for: indexPath) as! RegisterCredentialsCollectionViewCell
            
            cell.wellcomeLabel.text = "Solo un paso más, \(self.user.name ?? "") :)"
            
            return cell
        default:
            fatalError("ERROR_ Cell doesnt match")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 230)
    }
    
}
