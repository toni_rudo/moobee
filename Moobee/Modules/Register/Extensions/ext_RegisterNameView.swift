//
//  ext_RegisterDataView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 25/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

extension RegisterNameCollectionViewCell: RegisterNameView {
    func setName(_ string: String) {
        wellcomeLabel.text = string
    }
    
    func changeColor(from view: TextFieldLineView, to color: UIColor) {
        switch view {
        case .Name:
            nameView.backgroundColor = color
        case .Username:
            usernameView.backgroundColor = color
        }
    }
}
