//
//  RegisterData.swift
//  Moobee
//
//  Created by Toni García Alhambra on 25/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

protocol RegisterNameView: class {
    func setName(_ name: String)
    func changeColor(from view: TextFieldLineView, to color: UIColor)
}
