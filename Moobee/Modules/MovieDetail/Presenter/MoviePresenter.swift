//
//  MoviePresenter.swift
//  Moobee
//
//  Created by Toni García Alhambra on 19/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class MoviePresenter {
    
    // MARK: - Properties
    weak private var movieView: MovieView!
    
    // MARK: - Functions
    func attachView(view: MovieView){
        movieView = view
    }
    
    // MARK: - API
    
    func getMovieDetails(withID movieID: Int) {
        Movie.getMovieDetails(movieID: movieID) { (movie) in
            if self.movieView != nil {
                self.getImages(pathForBackdrop: movie.backdrop_path ?? "", pathForPoster: movie.poster_path ?? "")
                self.movieView.setInfo(from: movie)
            }
        }
    }
    
    func getCredtis(fromMovieID movieID: Int) {
        Credits.getCredits(movieID: movieID) { (credits) in
            if self.movieView != nil {
                self.movieView.setCast(credits.cast)
                
                var crew = [crew_childCredits]()
                for person in credits.crew { // Filtra solo los directores
                    if person.job == "Director" {
                        crew.append(person)
                    }
                }
                
                self.movieView.setCrew(crew)
                
                for cast in credits.cast {
                    Alamofire.request("\(Path.imagesSmallSize)\(cast.profile_path ?? "")").responseImage { response in
                        if let image = response.result.value {
                            cast.profile_pic = image
                        } else {
                            cast.profile_pic = #imageLiteral(resourceName: "profilePicPlaceholder")
                        }
                        
                        if self.movieView != nil {
                            self.movieView.reloadCast()
                        }
                    }
                }
                
                for crew in crew {
                    Alamofire.request("\(Path.imagesSmallSize)\(crew.profile_path ?? "")").responseImage { response in
                        if let image = response.result.value {
                            crew.profile_pic = image
                        } else {
                            crew.profile_pic = #imageLiteral(resourceName: "profilePicPlaceholder")
                        }
                        
                        if self.movieView != nil {
                            self.movieView.reloadCrew()
                        }
                    }
                }
            }
        }
    }
    
    func getImages(pathForBackdrop: String, pathForPoster: String) {
        Alamofire.request("\(Path.imagesOriginalSize)\(pathForBackdrop)").responseImage { response in
            if let image = response.result.value {
                if self.movieView != nil {
                    self.movieView.set(image: image, in: .backdrop)
                }
            }
        }
        Alamofire.request("\(Path.imagesSmallSize)\(pathForPoster)").responseImage { response in
            if let image = response.result.value {
                if self.movieView != nil {
                    self.movieView.set(image: image, in: .poster)
                }
            }
        }
    }
}
