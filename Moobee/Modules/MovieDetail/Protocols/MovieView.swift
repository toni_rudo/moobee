//
//  MovieView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 19/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

protocol MovieView: class {
    func setInfo(from movie: Movie)
    func setCast(_ cast: [cast_childCredtis])
    func setCrew(_ crew: [crew_childCredits])
    func set(image: UIImage, in imageView: ImagesPlaces)
    func reloadCast()
    func reloadCrew()
}
