//
//  MovieDetailViewController.swift
//  Moobee
//
//  Created by Toni García Alhambra on 15/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

// MARK: - Enums
enum ImagesPlaces {
    case backdrop
    case poster
}

// MARK: - Class
class MovieDetailViewController: UIViewController {
    // MARK: - Outlets
    // Images
    @IBOutlet weak var backdropImageView: UIImageView!
    @IBOutlet weak var posterImageView: UIImageView!
    
    // Labels
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var runtimeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var directingLabel: UILabel!
    @IBOutlet weak var castLabel: UILabel!
    
    // Collections
    @IBOutlet weak var directingCollectionView: UICollectionView!
    @IBOutlet weak var castCollectionView: UICollectionView!
    
    // Scrolls
    @IBOutlet weak var scrollView: UIScrollView!
    
    // Views
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var listsView: UIView!
    @IBOutlet weak var castView: UIView!
    
    // Buttons
    @IBOutlet weak var viewAllCastButton: UIButton!
    @IBOutlet weak var viewButton: UIButton!
    @IBOutlet weak var moobButton: UIButton!
    
    // Constrains
    @IBOutlet weak var topViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var directingHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var castHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Lets and vars
    // Constants
    private let moviePresenter = MoviePresenter()
    let columnLayoutCast = ColumnFlowLayout(
        cellsPerRow: 3,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    )
    let columnLayoutCrew = ColumnFlowLayout(
        cellsPerRow: 3,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    )
    
    // Variables
    var id: Int = 0
    var viewAll: Bool = false
    var castCellHeight: CGFloat = 0
    var navBarHidden: Bool = true
    
    var cast = [cast_childCredtis]() {
        didSet {
            castCollectionView.reloadData()
            
            castCollectionView.collectionViewLayout = columnLayoutCast
            castCellHeight = columnLayoutCast.itemHeight
        }
    }
    
    var crew = [crew_childCredits]() {
        didSet {
            directingCollectionView.reloadData()
            
            directingCollectionView.collectionViewLayout = columnLayoutCrew
        }
    }
    
    var parentController: CategoriesTableViewController?
    
    // MARK: - Life cicle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        castCollectionView.delegate = self
        castCollectionView.dataSource = self
        directingCollectionView.delegate = self
        directingCollectionView.dataSource = self
        
        castCollectionView.register(UINib(nibName: "CastCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "castCell")
        directingCollectionView.register(UINib(nibName: "CastCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "castCell")
        
        moviePresenter.attachView(view: self)
        
        if id != 0 {
            moviePresenter.getMovieDetails(withID: id)
            moviePresenter.getCredtis(fromMovieID: id)
        } else {
            navigationController?.popViewController(animated: true)
        }
        
        setUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.alpha = 1
        
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        topViewConstraint.constant = -44
        if scrollView.contentOffset.y == 0 {
            self.navigationController?.navigationBar.alpha = 0
        }
        if self.navigationController?.navigationBar.alpha == 1 {
            topViewConstraint.constant += -44
        }
    }
    
    // MARK: - Functions
    func setUI() {
        self.navigationController?.navigationBar.topItem?.title = ""
        
        posterImageView.layer.cornerRadius = 5
        
        viewButton.layer.cornerRadius = viewButton.frame.height / 2
        moobButton.layer.cornerRadius = moobButton.frame.height / 2
        
        setColors()
    }
    
    func setColors() {
        // Views
        mainView.backgroundColor = ColorsForDarkVersion.main
        listsView.backgroundColor = ColorsForDarkVersion.main
        castView.backgroundColor = ColorsForDarkVersion.main
        directingCollectionView.backgroundColor = ColorsForDarkVersion.main
        castCollectionView.backgroundColor = ColorsForDarkVersion.main
        self.view.backgroundColor = ColorsForDarkVersion.main
        
        // Labels
        titleLabel.textColor = UIColor.white
        dateLabel.textColor = ColorsForDarkVersion.emphasis
        runtimeLabel.textColor = UIColor.lightGray
        genreLabel.textColor = UIColor.lightGray
        descriptionLabel.textColor = UIColor.white
        directingLabel.textColor = UIColor.white
        castLabel.textColor = UIColor.white
        
        // Buttons
        viewAllCastButton.tintColor = ColorsForDarkVersion.emphasis
        moobButton.backgroundColor = ColorsForDarkVersion.emphasis
        viewButton.backgroundColor = ColorsForDarkVersion.emphasis
        
        // Navigation
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    // MARK: - Button functions
    @IBAction func viewAllCastButtonPressed(_ sender: UIButton) {
        if viewAll {
            viewAll = false
            viewAllCastButton.setTitle("VER TODOS", for: .normal)
        } else {
            viewAll = true
            viewAllCastButton.setTitle("VER MENOS", for: .normal)
        }
        
        castCollectionView.reloadData()
    }
}
