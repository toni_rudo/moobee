//
//  ext_MovieView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 19/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit
import Alamofire

extension MovieDetailViewController: MovieView {
    func set(image: UIImage, in imageView: ImagesPlaces) {
        switch imageView {
        case .backdrop:
            backdropImageView.image = image
        case .poster:
            posterImageView.image = image
        }
    }
    
    func setInfo(from movie: Movie) {
        self.title = movie.title
        titleLabel.text = movie.title
        dateLabel.text = movie.release_date
        runtimeLabel.text = "\(movie.runtime ?? 0)min"
        descriptionLabel.text = movie.overview
        if movie.genres.count != 0 {
            genreLabel.text = "\(movie.genres[0].name!) · "
        }
    }
    
    func setCast(_ cast: [cast_childCredtis]) {
        self.cast = cast
    }
    
    func reloadCast() {
        self.castCollectionView.reloadData()
    }
    
    func setCrew(_ crew: [crew_childCredits]) {
        self.crew = crew
    }
    
    func reloadCrew() {
        self.directingCollectionView.reloadData()
    }
}
