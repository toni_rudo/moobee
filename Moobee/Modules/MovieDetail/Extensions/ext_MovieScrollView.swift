//
//  ext_MovieScrollView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 20/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

extension MovieDetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView.convert(titleLabel.frame.origin, to: self.view).y < -200 && navBarHidden) {
            
            navBarHidden = false
            
            UIView.animate(withDuration: 0.3, animations: {
                self.navigationController?.navigationBar.alpha = 1
                self.navigationController?.navigationBar.layoutIfNeeded()
            }, completion: nil)
            
        } else if (scrollView.convert(titleLabel.frame.origin, to: self.view).y > -200 && !navBarHidden) {
            
            navBarHidden = true
            
            UIView.animate(withDuration: 0.3, animations: {
                self.navigationController?.navigationBar.alpha = 0
                self.navigationController?.navigationBar.layoutIfNeeded()
            }, completion: nil)
        }
    }
}
