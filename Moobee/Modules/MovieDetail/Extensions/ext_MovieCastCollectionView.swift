//
//  ext_MovieCastCollectionView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 21/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

extension MovieDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var cells: Int = 0
        var height: CGFloat = 0
        
        switch collectionView {
            case self.castCollectionView:
                
                if viewAll || cast.count < 9 {
                    
                    let rows: CGFloat = (CGFloat(cast.count) / CGFloat(3)).rounded(.up)
                    
                    // Se suma 5, que es la mitad del minimumLineSpacing
                    height = (castCellHeight + 10) * rows
                    
                    cells = cast.count
                } else {
                    height = (castCellHeight + 10) * 3
                    cells = 9
                }
            
                castHeightConstraint.constant = height
            
            case self.directingCollectionView:
                directingHeightConstraint.constant = castCellHeight
                cells = crew.count
            
            default: break
        }
        
        return cells
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "castCell", for: indexPath) as! CastCollectionViewCell
        
        cell.parentViewController = self
        cell.categoriesViewController = self.parentController
        
        switch collectionView {
            
        case self.castCollectionView:
            
            let node: Character = "/"
            let characterName = cast[indexPath.row].character!
            
            // Corta el string si tiene varios nombres. ej: Carol/Cap. Marvel -> Carol
            if let index = characterName.index(of: node) {
                let pos = characterName.distance(from: characterName.startIndex, to: index)
                cell.characterNameLabel.text = String((characterName.prefix(pos)))
            } else {
                cell.characterNameLabel.text = characterName
            }
            
            cell.nameLabel.text = cast[indexPath.row].name
            cell.name = cast[indexPath.row].name
            cell.button.tag = cast[indexPath.row].id ?? 0
            
            if let profile_pic = cast[indexPath.row].profile_pic {
                cell.profilePic.image = profile_pic
            }
            
        case self.directingCollectionView:
            cell.characterNameLabel.text = crew[indexPath.row].name
            cell.nameLabel.text = crew[indexPath.row].job
            cell.name = crew[indexPath.row].name
            cell.button.tag = crew[indexPath.row].id ?? 0
            
            if let profile_pic = crew[indexPath.row].profile_pic {
                cell.profilePic.image = profile_pic
            }
            
        default:
            print("ERROR_ table reloaded from \(collectionView)")
        }
        
        return cell
    }
}
