//
//  Login_LoginView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 15/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

extension LoginViewController: LoginView {
    func showLoginFields() {
        self.textLabelsView.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.textLabelsView.alpha = 1
        }, completion:  nil)
    }
    
    func animateLogo(_ animate: Bool) {
        if animate {
            logoAnimation.loopAnimation = true
            logoAnimation.play()
        }
        else {
            logoAnimation.loopAnimation = false
            logoAnimation.animationProgress = 1
        }
    }
    
    func changeColor(from field: LoginViewField, to color: UIColor) {
        switch field {
        case .UsernameField:
            usernameUnderlineView.layer.backgroundColor = color.cgColor
        case .PasswordField:
            passwordUnderlineView.layer.backgroundColor = color.cgColor
        }
    }
    
    func changeBackground(with path: [String]) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            let randomA = Int.random(in: 0 ..< self.backgroundImages.count)
            let randomB = Int.random(in: 0 ..< 20)
            
            let url = URL(string: path[randomB])
            let data = try? Data(contentsOf: url!)
            
            if let imageData = data {
                self.backgroundImages[randomA] = UIImage(data: imageData)!
            }
            
            let index = IndexPath(item: randomA, section: 0)
            self.backgroundCollectionView.reloadItems(at: [index])
            
            self.changeBackground(with: path)
        }
    }
}
