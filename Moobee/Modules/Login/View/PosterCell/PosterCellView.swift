//
//  PosterCellCollectionViewCell.swift
//  Moobee
//
//  Created by Toni García Alhambra on 13/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class PosterCellView: UICollectionViewCell {

    @IBOutlet private weak var posterImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setImage(_ image: UIImage) {
        posterImageView.image = image
    }
    
}
