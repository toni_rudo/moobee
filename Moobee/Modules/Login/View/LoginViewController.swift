//
//  LoginViewController.swift
//  Moobee
//
//  Created by Toni García Alhambra on 12/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit
import Lottie

enum LoginViewField {
    case UsernameField
    case PasswordField
}

class LoginViewController: UIViewController {
    
    // MARK: - outlets
    @IBOutlet weak var textLabelsView: UIView!
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var usernameUnderlineView: UIView!
    @IBOutlet weak var passwordUnderlineView: UIView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var backgroundCollectionView: UICollectionView!
    
    // MARK: - constants
    private let loginPresenter = LoginPresenter()
    let logoAnimation = LOTAnimationView(name: "film")
    
    // MARK: - variables
    var backgroundImages = [#imageLiteral(resourceName: "captainMarvel"), #imageLiteral(resourceName: "avatar"), #imageLiteral(resourceName: "arrival"), #imageLiteral(resourceName: "inAHearbeat"), #imageLiteral(resourceName: "harryPotter2"), #imageLiteral(resourceName: "her"), #imageLiteral(resourceName: "rompeRalph2"), #imageLiteral(resourceName: "bigHero6"), #imageLiteral(resourceName: "spiderman"), #imageLiteral(resourceName: "yourName"), #imageLiteral(resourceName: "thepursuitofhappyness"), #imageLiteral(resourceName: "matrixrealoaded")]
    var cell = [PosterCellView?]()
    var textLabelsViewOrigin: CGFloat = 0
    var reachability: Reachability?
    
    // MARK: - app cicle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginPresenter.attachView(view: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        backgroundCollectionView.delegate = self
        backgroundCollectionView.dataSource = self
        
        backgroundCollectionView.register(UINib(nibName: "PosterCell", bundle: nil), forCellWithReuseIdentifier: "posterCell")
        
        let columnLayout = ColumnFlowLayout(
            cellsPerRow: 3,
            minimumInteritemSpacing: 0,
            minimumLineSpacing: 0,
            sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        )
        
        
        backgroundCollectionView.collectionViewLayout = columnLayout
        
        reachability = Reachability.init()
        
        if reachability!.connection == .wifi {
            loginPresenter.getBackgroundImages()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        setUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        textLabelsViewOrigin = textLabelsView.frame.origin.y
        
        if loginPresenter.getUser() {
            let mainVC = TabBarViewController.instantiate(fromAppStoryboard: .TabBar)
            self.present(mainVC, animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - ui functions
    @objc func keyboardWillShow(notification: NSNotification) {
        if self.textLabelsView.frame.origin.y == textLabelsViewOrigin {
            self.textLabelsView.frame.origin.y -= 100
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.textLabelsView.frame.origin.y != textLabelsViewOrigin {
            self.textLabelsView.frame.origin.y = textLabelsViewOrigin
        }
    }
    
    func setUI() {
        textLabelsView.layer.cornerRadius = 7.5
        
        logoAnimation.frame = CGRect(x: 0, y: 0, width: 55, height: 50)
        logoView.addSubview(logoAnimation)
        
        loginButton.layer.cornerRadius = loginButton.frame.height / 2
        
        setColors()
    }
    
    func setColors() {
        loginButton.layer.backgroundColor = ColorsForDarkVersion.emphasis.cgColor
        textLabelsView.backgroundColor = ColorsForDarkVersion.main.withAlphaComponent(0.95)
        
        usernameTextField.attributedPlaceholder = NSAttributedString(string: "usuario",
                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "contraseña",
                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
    }
    
    // MARK: - buttons
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        let username = usernameTextField.text
        let password = passwordTextField.text
        
        loginPresenter.callLogin(username, password) { (logged) in
            if logged {
                let mainVC = TabBarViewController.instantiate(fromAppStoryboard: .TabBar)
                self.present(mainVC, animated: true)
            }
            
            self.animateLogo(false)
        }
    }
    
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2, animations: {
            self.textLabelsView.alpha = 0
        }, completion:  {
            (value: Bool) in
            self.textLabelsView.isHidden = true
        })
        
        let registerVC = RegisterViewController.instantiate(fromAppStoryboard: .Register)
//        registerVC.loginView = self
        registerVC.modalPresentationStyle = .overCurrentContext

        present(registerVC, animated: true)
    }
}
