//
//  LoginView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 15/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

protocol LoginView: class {
    func changeBackground(with path: [String])
    func changeColor(from field: LoginViewField, to color: UIColor)
    func animateLogo(_ animate: Bool)
    func showLoginFields()
}
