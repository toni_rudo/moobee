//
//  LoginPresenter.swift
//  Moobee
//
//  Created by Toni García Alhambra on 12/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class LoginPresenter {
    
    // MARK: - Properties
    weak private var loginView: LoginView!
    
    // MARK: - Functions
    func attachView(view: LoginView){
        loginView = view
    }
    
    func getBackgroundImages() {
        DiscoveredMovie.getPosterPaths { (posters) in
            self.loginView.changeBackground(with: posters)
        }
    }
    
    func callLogin(_ user: String?, _ password: String?, completionHandler: @escaping ((Bool) -> Void)) {
        var postCall = true
        
        loginView.animateLogo(true)
        
        loginView.changeColor(from: .UsernameField, to: UIColor.white)
        loginView.changeColor(from: .PasswordField, to: UIColor.white)
        
        if user == "" {
            loginView.changeColor(from: .UsernameField, to: UIColor.red)
            loginView.animateLogo(false)
            postCall = false
        }
        if password == "" {
            loginView.changeColor(from: .PasswordField, to: UIColor.red)
            loginView.animateLogo(false)
            postCall = false
        }
        
        if postCall {
            LoginRoot.login(username: user!, password: password!) { (result) in
                switch result["code"] as! Int {
                case 200:
                    let defaults = UserDefaults.standard
                    let user = result["user"] as! LoginRoot
                    
                    defaults.set(user.data?.token, forKey: UserDafultsKeys.token)
                    
                    completionHandler(true)
                case 400:
                    self.loginView.changeColor(from: .PasswordField, to: UIColor.red)
                    completionHandler(false)
                case 404:
                    self.loginView.changeColor(from: .UsernameField, to: UIColor.red)
                    completionHandler(false)
                case 500:
                    print("ERROR_ with code \(result) login user: Internal Server Error")
                    completionHandler(false)
                default:
                    print("ERROR_ with code \(result) login user")
                    completionHandler(false)
                }
            }
        }
    }
    
    func getUser() -> Bool {
        let defaults = UserDefaults.standard
        
        if defaults.string(forKey: UserDafultsKeys.token) != nil {
            return true
        } else {
            return false
        }
    }
}
