//
//  PeopleDetailProtocol.swift
//  Moobee
//
//  Created by Toni García Alhambra on 26/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class PeopleDetailPresenter {
    // MARK: - Properties
    weak private var peopleView: PeopleView!
    
    // MARK: - Functions
    func attachView(view: PeopleView){
        peopleView = view
    }
    
    func getMoviesCredits(withID id: Int) {
        PersonCredits.getCreditsCasting(movieID: id) { (credits) in
            self.peopleView.setMovies(credits)
        }
        
        PersonCredits.getCreditsDirecting(movieID: id) { (credits) in
            self.peopleView.setMovies(credits)
        }
    }
    
    func setPosterImage(in movie: Movie) {
        Alamofire.request("\(Path.imagesSmallSize)\(movie.poster_path ?? "")").responseImage { response in
            if let image = response.result.value {
                movie.smallPosterImage = image
            }
            
            if self.peopleView != nil {
                self.peopleView.reloadMovies()
            }
        }

    }
}
