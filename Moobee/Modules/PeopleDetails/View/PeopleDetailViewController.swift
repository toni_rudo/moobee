//
//  PeopleViewController.swift
//  Moobee
//
//  Created by Toni García Alhambra on 26/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class PeopleDetailViewController: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var moviesCollectionView: UICollectionView!
    
    let peoplePresenter = PeopleDetailPresenter()
    
    var name: String?
    var image: UIImage?
    var parentController: CategoriesTableViewController?
    
    var id: Int = 0 {
        didSet {
            if id != 0 {
                peoplePresenter.getMoviesCredits(withID: id)
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    var movies = [Movie]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        peoplePresenter.attachView(view: self)
        
        moviesCollectionView.delegate = self
        moviesCollectionView.dataSource = self
        
        setUI()
    }
    
    func setUI() {
        profileImageView.layer.cornerRadius = 10
        
        let columnLayout = ColumnFlowLayout(
            cellsPerRow: 3,
            minimumInteritemSpacing: 10,
            minimumLineSpacing: 10,
            sectionInset: UIEdgeInsets(top: 20, left: 15, bottom: 60, right: 15)
        )
        
        moviesCollectionView.collectionViewLayout = columnLayout
        
        nameLabel.text = name
        profileImageView.image = image
        
        moviesCollectionView.register(UINib(nibName: "MovieCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "movieCollectionCell")
        
        setColors()
    }
    
    func setColors() {
        self.view.backgroundColor = ColorsForDarkVersion.main
        moviesCollectionView.backgroundColor = ColorsForDarkVersion.dark
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
