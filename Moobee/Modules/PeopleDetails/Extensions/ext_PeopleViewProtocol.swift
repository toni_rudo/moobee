//
//  ext_PeopleViewProtocol.swift
//  Moobee
//
//  Created by Toni García Alhambra on 26/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import Foundation

extension PeopleDetailViewController: PeopleView {
    func setMovies(_ credits: [Movie]) {
        for credit in credits {
            if !movies.contains(where: {$0.id == credit.id}) {
                self.movies.append(credit)
                self.peoplePresenter.setPosterImage(in: credit)
            }
        }
    }
    
    func reloadMovies() {
        self.moviesCollectionView.reloadData()
    }
}
