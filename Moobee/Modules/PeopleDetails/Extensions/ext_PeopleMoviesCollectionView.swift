//
//  ext_PeopleMoviesCollectionView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 26/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

extension PeopleDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if movies.count > 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCollectionCell", for: indexPath) as! MovieCollectionViewCell
            
            cell.posterImageView.image = movies[indexPath.row].smallPosterImage
            cell.titleLabel.text = movies[indexPath.row].title
            cell.cellButton.tag = movies[indexPath.row].id ?? 0
            cell.parentController = self.parentController
            cell.calledByActor = true
            cell.peopleDetailVC = self
            
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
}
