//
//  PeopleView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 26/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import Foundation

protocol PeopleView: class {
    func setMovies(_ credits: [Movie])
    func reloadMovies()
}
