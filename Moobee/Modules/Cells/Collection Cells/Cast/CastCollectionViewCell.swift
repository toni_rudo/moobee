//
//  CastCollectionViewCell.swift
//  Moobee
//
//  Created by Toni García Alhambra on 21/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class CastCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var characterNameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    
    var name: String?
    var parentViewController: MovieDetailViewController?
    var categoriesViewController: CategoriesTableViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUI()
    }
    
    func setUI(){
        profilePic.layer.cornerRadius = 10
    }
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        let peopleVC = PeopleDetailViewController.instantiate(fromAppStoryboard: .PeopleDetail)
        peopleVC.modalPresentationStyle = .overCurrentContext
        
        peopleVC.name = name
        peopleVC.image = profilePic.image
        peopleVC.id = button.tag
        peopleVC.parentController = categoriesViewController
        
        parentViewController?.present(peopleVC, animated: true)
    }
}
