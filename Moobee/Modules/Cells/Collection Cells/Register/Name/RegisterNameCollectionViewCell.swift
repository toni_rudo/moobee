//
//  NameCollectionViewCell.swift
//  Moobee
//
//  Created by Toni García Alhambra on 24/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

enum TextFieldLineView {
    case Name
    case Username
}

class RegisterNameCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var wellcomeLabel: UILabel!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var usernameView: UIView!
    
    private let registerNamePresenter = RegisterNamePresenter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        registerNamePresenter.attachView(view: self)
        
        nameTextField.becomeFirstResponder()
        
        setUI()
    }
    
    func setUI() {
        nameTextField.setLeftImage(#imageLiteral(resourceName: "man"))
        usernameTextField.setLeftImage(#imageLiteral(resourceName: "at"))
        
        setColors()
    }
    
    func setColors() {
        wellcomeLabel.textColor = ColorsForDarkVersion.emphasis
        
        nameTextField.setPlaceholderText(color: UIColor.gray)
        usernameTextField.setPlaceholderText(color: UIColor.gray)
    }
    
    @IBAction func nameChanged(_ sender: UITextField) {
        registerNamePresenter.checkName(nameField: sender)
    }
}
