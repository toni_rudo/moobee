//
//  RegisterCredentialsCollectionViewCell.swift
//  Moobee
//
//  Created by Toni García Alhambra on 25/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class RegisterCredentialsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var wellcomeLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailLineView: UIView!
    @IBOutlet weak var passwordLineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUI()
    }
    
    func setUI() {
        emailTextField.setLeftImage(#imageLiteral(resourceName: "mail"))
        passwordTextField.setLeftImage(#imageLiteral(resourceName: "padlock"))
        
        setColors()
    }
    
    func setColors() {
        wellcomeLabel.textColor = ColorsForDarkVersion.emphasis
        
        emailTextField.setPlaceholderText(color: UIColor.gray)
        passwordTextField.setPlaceholderText(color: UIColor.gray)
    }

}
