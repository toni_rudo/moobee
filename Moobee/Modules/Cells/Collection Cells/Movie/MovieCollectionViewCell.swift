//
//  MovieCollectionViewCell.swift
//  Moobee
//
//  Created by Toni García Alhambra on 14/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backgroundLayerView: UIView!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var backgroundTitleView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cellButton: UIButton!
    
    var parentController: CategoriesTableViewController?
    var calledByActor: Bool = false
    var peopleDetailVC: PeopleDetailViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUI()
    }
    
    func setUI(){
        backgroundLayerView.layer.cornerRadius = 5
        let gradient = createGradientLayer(toView: backgroundTitleView, fromColor: UIColor.black.withAlphaComponent(0).cgColor, toColor: UIColor.black.withAlphaComponent(1).cgColor)
        backgroundTitleView.layer.addSublayer(gradient)
        backgroundTitleView.bringSubviewToFront(titleLabel)
    }
    
    @IBAction func cellButtonPressed(_ sender: UIButton) {
        let movieVC = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
        movieVC.id = cellButton.tag
        movieVC.parentController = parentController
        
        if calledByActor {
            parentController?.navigationController?.popToRootViewController(animated: false)
            peopleDetailVC?.dismiss(animated: true, completion: nil)
        }
        
        parentController?.navigationController?.pushViewController(movieVC, animated: true)
    }
}
