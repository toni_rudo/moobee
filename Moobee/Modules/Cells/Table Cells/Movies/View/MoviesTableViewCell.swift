//
//  MoviesTableViewCell.swift
//  Moobee
//
//  Created by Toni García Alhambra on 14/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class MoviesTableViewCell: UITableViewCell {

    @IBOutlet weak var moviesCollectionView: UICollectionView!
    
    var movies = [Movie](){
        didSet {
            self.moviesCollectionView.reloadData()
        }
    }
    
    var parentController: CategoriesTableViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        moviesCollectionView.delegate = self
        moviesCollectionView.dataSource = self
        
        moviesCollectionView.register(UINib(nibName: "MovieCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "movieCollectionCell")
        
        setColors()
    }
    
    func setColors() {
        moviesCollectionView.backgroundColor = ColorsForDarkVersion.main
    }
}
