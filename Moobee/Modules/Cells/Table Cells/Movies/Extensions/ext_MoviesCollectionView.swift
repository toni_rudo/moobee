//
//  ext_MoviesTableView.swift
//  Moobee
//
//  Created by Toni García Alhambra on 15/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

extension MoviesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCollectionCell", for: indexPath) as! MovieCollectionViewCell
        
        if movies.count > 0 {
            cell.titleLabel.text = movies[indexPath.row].title
            cell.posterImageView.image = movies[indexPath.row].smallPosterImage
            cell.cellButton.tag = movies[indexPath.row].id ?? 0
            cell.parentController = self.parentController
        }
        
        return cell
    }
}
