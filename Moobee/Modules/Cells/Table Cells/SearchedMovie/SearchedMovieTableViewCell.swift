//
//  SearchedMovieTableViewCell.swift
//  Moobee
//
//  Created by Toni García Alhambra on 22/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class SearchedMovieTableViewCell: UITableViewCell {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var cellButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUI()
    }
    
    func setUI() {
        posterImageView.layer.cornerRadius = 5
        
        setColors()
    }
    
    func setColors() {
        self.backgroundColor? = ColorsForDarkVersion.main
        titleLabel.textColor = UIColor.white
        dateLabel.textColor = ColorsForDarkVersion.emphasis
    }
}
