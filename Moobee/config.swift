//
//  config.swift
//  Moobee
//
//  Created by Toni García Alhambra on 12/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

struct Path {
    static let movieURL = "https://api.themoviedb.org/3"
    static let imagesOriginalSize = "https://image.tmdb.org/t/p/original"
    static let imagesSmallSize = "https://image.tmdb.org/t/p/w300"
    static let backendURL = "https://moo-bee.herokuapp.com"
}

struct API_key {
    static let movie = "e439a792b107ab5c326080f6535691aa"
}

struct ColorsForDarkVersion {
    static let main = UIColor(red:0.01, green:0.17, blue:0.26, alpha:1.0)
    static let secondary = UIColor(red:0.02, green:0.25, blue:0.37, alpha:1.0)
    static let light = UIColor(red:0.07, green:0.32, blue:0.45, alpha:1.0)
    static let dark = UIColor(red:0.01, green:0.10, blue:0.15, alpha:1.0)
    static let emphasis = UIColor(red:1.00, green:0.84, blue:0.00, alpha:1.0)
}

struct UserDafultsKeys {
    static let token = "token"
}
