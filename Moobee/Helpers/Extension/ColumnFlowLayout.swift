//
//  ColumnFlowLayout.swift
//  Moobee
//
//  Created by Toni García Alhambra on 13/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

enum Orientation {
    case Height
    case Width
}

class ColumnFlowLayout: UICollectionViewFlowLayout {
    
    let cellsPerRow: Int
    
    var itemMultiplier: CGFloat = 1.48
    var itemHeight: CGFloat = 0
    var aspectRatio: [Orientation: Int] = [.Height: 0, .Width: 0]
    
    override var itemSize: CGSize {
        get {
            guard let collectionView = collectionView else { return super.itemSize }
            let marginsAndInsets = sectionInset.left + sectionInset.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
            let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
            
            if self.aspectRatio[.Height] != 0 {
                itemMultiplier = CGFloat(aspectRatio[.Height]! / aspectRatio[.Width]!)
            }
            
            self.itemHeight = CGFloat(itemWidth * itemMultiplier)
            return CGSize(width: itemWidth, height: itemHeight)
        }
        set {
            super.itemSize = newValue
        }
    }
    
    init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        self.cellsPerRow = cellsPerRow
        
        super.init()
        
        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds != collectionView?.bounds
        return context
    }
}
