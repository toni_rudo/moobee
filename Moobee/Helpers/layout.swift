//
//  layout.swift
//  Moobee
//
//  Created by Toni García Alhambra on 12/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

func addBlur(to imageView: UIImageView) {
    let darkBlur = UIBlurEffect(style: UIBlurEffect.Style.dark)
    let blurView = UIVisualEffectView(effect: darkBlur)
    blurView.frame = imageView.bounds
    blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    blurView.alpha = 0.5
    
    imageView.addSubview(blurView)
}

func createGradientLayer(toView view: UIView, fromColor topColor: CGColor, toColor bottomColor: CGColor) -> CAGradientLayer {
    let gradientLayer = CAGradientLayer()
    
    gradientLayer.frame = view.bounds
    
    gradientLayer.colors = [topColor, bottomColor]
    
    return gradientLayer
}

