//
//  API.swift
//  Moobee
//
//  Created by Toni García Alhambra on 12/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import Foundation
import Alamofire

class API{
    static let shared = API()
    public static var manager: Alamofire.SessionManager = Alamofire.SessionManager()
    
    func call(_ URLRequest: URLRequestConvertible, success: @escaping (Int,Any) -> Void, failure: @escaping (Int,Any) -> Void) -> Void {
        API.manager.request(URLRequest).responseJSON { object in
            switch object.result {
            case .success(_):
                print("----------- LLAMADA CORRECTA -------------")
                print("LLAMADA: \(URLRequest.urlRequest?.url?.absoluteString ?? "")")
                print("CODIGO: \(object.response?.statusCode ?? 0)")
                print("DESCRIPCION: \(object.result.description )")
                print("VALOR: \(String(describing: object.result.value))")
                print("------------------------------------------")
                
                success(object.response?.statusCode ?? 0, object.result.value as AnyObject)
                
            case .failure(_):
                print("----------- ERROR EN LA LLAMADA -------------")
                print("LLAMADA: \(URLRequest.urlRequest?.url?.absoluteString ?? "")")
                print("CODIGO: \(object.response?.statusCode ?? 0)")
                print("DESCRIPCION: \(object.result.description )")
                print("VALOR: \(String(describing: object.result.value))")
                print("------------------------------------------")
                
//                failure(-1, object.result.value ?? -1)
//                failure((object.response?.statusCode)!, object.response?.statusCode ?? -1)
            }
        }
    }
}
