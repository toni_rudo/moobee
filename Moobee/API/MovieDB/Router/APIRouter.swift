//
//  APIRouter.swift
//  Moobee
//
//  Created by Toni García Alhambra on 12/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import Alamofire

enum APIRouter: URLRequestConvertible {
    
    case discoverMovie([String: AnyObject])
    case moviesInTheatres([String: AnyObject])
    case topRatedMovies([String: AnyObject])
    case movieDetails(Int, [String: AnyObject])
    case credits(Int, [String: AnyObject])
    case searchMovie([String: AnyObject])
    case personMovieCredits(Int, [String: AnyObject])
    
    var method: Alamofire.HTTPMethod {
        return .get
    }
    
    var path: String {
        switch self {
        case .discoverMovie:
            return "/discover/movie"
        case .moviesInTheatres:
            return "/movie/now_playing"
        case .topRatedMovies:
            return "/movie/top_rated"
        case .movieDetails:
            return "/movie"
        case .credits:
            return "/movie"
        case .searchMovie:
            return "/search/movie"
        case.personMovieCredits:
            return "/person"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: Path.movieURL)!
        
        switch self {
        case .discoverMovie(var parameters),
             .moviesInTheatres(var parameters),
             .topRatedMovies(var parameters),
             .searchMovie(var parameters):
            
            var urlRequest = URLRequest(url: url.appendingPathComponent(path))
            urlRequest.httpMethod = method.rawValue
            
            parameters["api_key"] = API_key.movie as AnyObject
            return try Alamofire.URLEncoding.queryString.encode(urlRequest, with: parameters)
            
        case .movieDetails(let id, var parameters):
            var urlRequest = URLRequest(url: url.appendingPathComponent("\(path)/\(id)"))
            urlRequest.httpMethod = method.rawValue
            
            parameters["api_key"] = API_key.movie as AnyObject
            
            return try Alamofire.URLEncoding.queryString.encode(urlRequest, with: parameters)
            
        case .credits(let id, var parameters):
            var urlRequest = URLRequest(url: url.appendingPathComponent("\(path)/\(id)/credits"))
            urlRequest.httpMethod = method.rawValue
            
            parameters["api_key"] = API_key.movie as AnyObject
            
            return try Alamofire.URLEncoding.queryString.encode(urlRequest, with: parameters)
        case .personMovieCredits(let id, var parameters):
            var urlRequest = URLRequest(url: url.appendingPathComponent("\(path)/\(id)/movie_credits"))
            urlRequest.httpMethod = method.rawValue
            
            parameters["api_key"] = API_key.movie as AnyObject
            
            return try Alamofire.URLEncoding.queryString.encode(urlRequest, with: parameters)
        }
    }
}
