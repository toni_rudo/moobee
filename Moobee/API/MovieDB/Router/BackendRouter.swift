//
//  BackendRouter.swift
//  Moobee
//
//  Created by Toni García Alhambra on 22/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import Alamofire

enum BackendRouter: URLRequestConvertible {
    
    case login([String: AnyObject])
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .login:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .login:
            return "/login"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: Path.backendURL)!
        
        switch self {
        case .login(let parameters):
            var urlRequest = URLRequest(url: url.appendingPathComponent(path))
            urlRequest.httpMethod = method.rawValue
            
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: parameters)
        }
    }
}
