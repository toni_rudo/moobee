//
//  Credtis.swift
//  Moobee
//
//  Created by Toni García Alhambra on 21/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import ObjectMapper
import Alamofire

class Credits: Mappable {
    var id: Int?
    var cast = [cast_childCredtis]()
    var crew = [crew_childCredits]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        cast <- map["cast"]
        crew <- map["crew"]
    }
}

class cast_childCredtis: Mappable {
    var name,character,credit_id,profile_path: String?
    var gender,order,cast_id,id: Int?
    var profile_pic: UIImage?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        name <- map["name"]
        character <- map["character"]
        credit_id <- map["credit_id"]
        profile_path <- map["profile_path"]
        gender <- map["gender"]
        order <- map["order"]
        cast_id <- map["cast_id"]
        id <- map["id"]
    }
}

class crew_childCredits: Mappable {
    var name,department,job,credit_id,profile_path: String?
    var gender,id: Int?
    var profile_pic: UIImage?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        name <- map["name"]
        department <- map["department"]
        job <- map["job"]
        credit_id <- map["credit_id"]
        profile_path <- map["profile_path"]
        gender <- map["gender"]
        id <- map["id"]
    }
}

extension Credits {
    class func getCredits(movieID: Int, completionHandler: @escaping ((Credits) -> Void)) {
        let params: [String: Any] =
            ["language": "es-ES"]
        
        API().call(APIRouter.credits(movieID, params as [String : AnyObject]), success: { (code, object) in
            if let credits = Mapper<Credits>().map(JSON: object as! [String: Any]) {
                completionHandler(credits)
            }
        }) { (code, object) in
            print("ERROR_ Could not receive data from movieDB: \(code)")
        }
    }
}
