//
//  PersonCredits.swift
//  Moobee
//
//  Created by Toni García Alhambra on 26/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import ObjectMapper

class PersonCredits: Mappable {
    var id: Int?
    var cast = [Movie]()
    var crew = [Movie]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        cast <- map["cast"]
        crew <- map["crew"]
    }
}

extension PersonCredits {
    class func getCredits(movieID: Int, completionHandler: @escaping ((PersonCredits) -> Void)) {
        let params: [String: Any] =
            ["language": "es-ES"]
        
        API().call(APIRouter.personMovieCredits(movieID, params as [String : AnyObject]), success: { (code, object) in
            if let credits = Mapper<PersonCredits>().map(JSON: object as! [String: Any]) {
                completionHandler(credits)
            }
        }) { (code, object) in
            print("ERROR_ Could not receive data from movieDB: \(code)")
        }
    }
    
    class func getCreditsCasting(movieID: Int, completionHandler: @escaping (([Movie]) -> Void)) {
        PersonCredits.getCredits(movieID: movieID) { (credits) in
            
            let castWithPoster = credits.cast.filter( {$0.poster_path != nil }).map({ return $0 })
            let creditsSorted = castWithPoster.sorted(by: { Int(String($0.release_date?.prefix(4) ?? "0")) ?? 0 > Int(String($1.release_date?.prefix(4) ?? "0")) ?? 0 })
            
            completionHandler(creditsSorted)
        }
    }
    
    class func getCreditsDirecting(movieID: Int, completionHandler: @escaping (([Movie]) -> Void)) {
        PersonCredits.getCredits(movieID: movieID) { (credits) in
            let crew = credits.crew
            
            let crewDirecting = crew.filter( {$0.job == "Director" }).map({ return $0 })
            let crewWithPoster = crewDirecting.filter( {$0.poster_path != nil }).map({ return $0 })
            
            completionHandler(crewWithPoster)
        }
    }
}
