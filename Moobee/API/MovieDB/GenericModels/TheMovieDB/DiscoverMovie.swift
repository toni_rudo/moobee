//
//  GenericModels.swift
//  Moobee
//
//  Created by Toni García Alhambra on 12/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import ObjectMapper
import Alamofire

class DiscoveredMovie: Mappable {
    var results = [Movie]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        results <- map["results"]
    }
}

// MARK: - Functions (getters)

extension DiscoveredMovie {
    class func getDiscoveredMovies(completionHandler: @escaping ((DiscoveredMovie) -> Void)) {
        let params: [String: Any] =
            ["language": "es-ES",
             "sort_by": "popularity.desc",
             "include_adult": "false",
             "include_video": "false",
             "page": 1]
        
        API().call(APIRouter.discoverMovie(params as [String : AnyObject]), success: { (code, object) in
            if let discoveredMovies = Mapper<DiscoveredMovie>().map(JSON: object as! [String: Any]) {
                completionHandler(discoveredMovies)
            }
        }) { (code, object) in
            print("ERROR_ Could not receive data from movieDB: \(code)")
        }
    }
    
    class func getPosterPaths(completionHandler: @escaping (([String]) -> Void)) {
        var posterPaths = [String]()
        
        DiscoveredMovie.getDiscoveredMovies { (movies) in
            for movie in movies.results {
                let path = "\(Path.imagesOriginalSize)\(movie.poster_path ?? "")"
                posterPaths.append(path)
            }
            completionHandler(posterPaths)
        }
    }
}
