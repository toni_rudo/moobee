//
//  MovieNowPlaying.swift
//  Moobee
//
//  Created by Toni García Alhambra on 14/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import ObjectMapper
import Alamofire

class MovieNowPlaying: Mappable {
    var results = [Movie]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        results <- map["results"]
    }
}

// MARK: - Functions (getters)

extension MovieNowPlaying {
    class func getMoviesInTheatres(completionHandler: @escaping ((MovieNowPlaying) -> Void)) {
        let params: [String: Any] =
            ["language": "es-ES",
             "page": 1]
        
        API().call(APIRouter.moviesInTheatres(params as [String : AnyObject]), success: { (code, object) in
            if let playingMovies = Mapper<MovieNowPlaying>().map(JSON: object as! [String: Any]) {
                completionHandler(playingMovies)
            }
        }) { (code, object) in
            print("ERROR_ Could not receive data from movieDB: \(code)")
        }
    }
}
