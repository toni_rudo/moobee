//
//  Movie.swift
//  Moobee
//
//  Created by Toni García Alhambra on 14/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import ObjectMapper
import Alamofire

class Movie: Mappable {
    var popularity,vote_average: Double?
    var poster_path,title,overview,release_date,original_title,backdrop_path,original_language, job: String?
    var vote_count,id,runtime: Int?
    var video,adult: Bool?
    var genre_ids = [Int]()
    var smallPosterImage: UIImage?
    var genres = [genres_childMovies]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        popularity          <- map["popularity"]
        vote_average        <- map["vote_average"]
        poster_path         <- map["poster_path"]
        title               <- map["title"]
        overview            <- map["overview"]
        release_date        <- map["release_date"]
        original_title      <- map["original_title"]
        backdrop_path       <- map["backdrop_path"]
        original_language   <- map["original_language"]
        vote_count          <- map["vote_count"]
        id                  <- map["id"]
        video               <- map["video"]
        adult               <- map["adult"]
        genre_ids           <- map["genre_ids"]
        runtime             <- map["runtime"]
        genres              <- map["genres"]
        job                 <- map["job"]
    }
}

class genres_childMovies: Mappable {
    var id: Int?
    var name: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id   <- map["id"]
        name <- map["name"]
    }
}

class SearchedMovies: Mappable {
    var results = [Movie]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        results <- map["results"]
    }
}

extension Movie {
    class func getMovieDetails(movieID: Int, completionHandler: @escaping ((Movie) -> Void)) {
        let params: [String: Any] =
            ["language": "es-ES"]
        
        API().call(APIRouter.movieDetails(movieID, params as [String : AnyObject]), success: { (code, object) in
            if let movie = Mapper<Movie>().map(JSON: object as! [String: Any]) {
                completionHandler(movie)
            }
        }) { (code, object) in
            print("ERROR_ Could not receive data from movieDB: \(code)")
        }
    }
    
    class func searchMovies(withString string: String, completionHandler: @escaping (([Movie]) -> Void)) {
        let params: [String: Any] =
            ["language": "es-ES",
             "query": string]
        
        API().call(APIRouter.searchMovie(params as [String : AnyObject]), success: { (code, object) in
            if let movie = Mapper<SearchedMovies>().map(JSON: object as! [String: Any]) {
                completionHandler(movie.results)
            }
        }) { (code, object) in
            print("ERROR_ Could not receive data from movieDB: \(code)")
        }
    }
}
