//
//  Login.swift
//  Moobee
//
//  Created by Toni García Alhambra on 22/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import ObjectMapper

class LoginRoot: Mappable {
    var error: Bool?
    var data: Token?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error <- map["error"]
        data <- map["data"]
    }
}

class Token: Mappable {
    var token: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        token <- map["token"]
    }
}

extension LoginRoot {
    class func login(username: String, password: String, completionHandler: @escaping (([String: Any]) -> Void)) {
        let params: [String: Any] =
            ["userName": username,
             "password": password]
        
        API().call(BackendRouter.login(params as [String : AnyObject]), success: { (code, object) in
            if let user = Mapper<LoginRoot>().map(JSON: object as! [String: Any]) {
                let result: [String: Any] =
                    ["code": code,
                     "user": user]
                
                completionHandler(result)
            }
        }) { (code, object) in
            
            let result: [String: Int] =
                ["code": code]
            
            completionHandler(result)
        }
    }
}
